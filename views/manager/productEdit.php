<?php
session_start();
require_once("../../vendor/autoload.php");
include('../templateLayout/information.php');

use App\Authentication;
use App\Utility\Utility;

if($_SESSION['role_status']==1){
    $auth= new Authentication();
    $status = $auth->setData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../panel/login.php');
        return;
    }
}
else {
    Utility::redirect('../panel/login.php');
}
$object= new \App\Product_lookup();
$object->setData($_GET);
$oneData = $object->view();

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title;?></title>

    <?php include('../templateLayout/css/css.php');?>

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include ('../templateLayout/managerNavigation.php');?>
    <!-- Navigation -->


    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Product Details</h1>
            </div>
            
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit products details
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3">
                                <form role="form" action="<?php echo base_url; ?>controller/manager/update.php" method="post">
                                    <div class="form-group">
                                        <label>Product Name</label>
                                        <input class="form-control" type="text" placeholder="Product Name" name="product_name" value="<?php echo $oneData->product_name?>">
                                    </div>
                                    <div class="form-group">
                                        <label>Product Price</label>
                                        <input class="form-control" placeholder="Product Price" type="number" name="unit_price" value="<?php echo $oneData->unit_price?>">
                                    </div>
                                    <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
                                    <button type="submit" class="btn btn-info">Update</button>
                                    
                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include('../templateLayout/script/script.php');?>


</body>

</html>
