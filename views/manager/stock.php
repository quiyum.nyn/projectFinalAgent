<?php
session_start();
require_once("../../vendor/autoload.php");
include('../templateLayout/information.php');
use App\Authentication;
use App\Utility\Utility;
if($_SESSION['role_status']==1){
    $auth= new Authentication();
    $status = $auth->setData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../panel/login.php');
        return;
    }
}
else {
    Utility::redirect('../panel/login.php');
}
$object= new \App\Inventory();
$allData = $object->stock();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title;?></title>
    <?php include('../templateLayout/css/tableCss.php');?>


</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include ('../templateLayout/managerNavigation.php');?>
    <!-- Navigation -->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Stock</h1>
            </div>
            <?php

            use App\Message\Message;


            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <div class='alert alert-info alert-dismissable' id='message' style='color: white; background: #6d86d3; text-align: center; font-family: Pristina; font-weight: 200 ;font-size: 20px;'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
                                        $msg.
                                    </div>
                                </div>
                            </div>
                        </div>";
            }

            ?>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Stock details
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr style="width: 100%">
                                    <th style='text-align: center;width: 7%'>Serial</th>
                                    <th style='text-align: center;width: 10%'>Product Name</th>
                                    <th style='text-align: center;width: 10%'>Debit Quantity</th>
                                    <th style='text-align: center;width: 10%'>Credit Quantity</th>
                                    <th style='text-align: center;width: 10%'>Balance Quantity</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach($allData as $oneData){
                                    echo" <tr>
                                    <td style='text-align: center'>$serial</td>
                                    <td style='text-align: center'>$oneData->productName</td>
                                    <td style='text-align: center'>$oneData->DebitQuantity</td>
                                    <td style='text-align: center'>$oneData->CreditQuantity</td>
                                    <td style='text-align: center'>$oneData->BalanceQuantity</td>
                                </tr>";
                                    $serial++;
                                }?>



                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include('../templateLayout/script/scriptMessege.php');?>
<?php include('../templateLayout/script/tableScript.php');?>


</body>

</html>
