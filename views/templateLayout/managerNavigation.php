<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url; ?>views/manager/index.php">Manager</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">

        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="<?php echo base_url; ?>views/manager/profile.php"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url; ?>controller/authentication/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                <li>
                    <a href="<?php echo base_url; ?>views/manager/purchaseDetails.php"><i class="fa fa-table fa-fw"></i>Purchase Products</a>
                </li>
                <li>
                    <a href="<?php echo base_url; ?>views/manager/orderedList.php"><i class="fa fa-table fa-fw"></i> Order List</a>
                </li>
                <li>
                    <a href="<?php echo base_url; ?>views/manager/deliveryList.php"><i class="fa fa-table fa-fw"></i> Delivered List</a>
                </li>
                <li>
                    <a href="<?php echo base_url; ?>views/manager/purchaseList.php"><i class="fa fa-table fa-fw"></i> Purchase List</a>
                </li>
                <li>
                    <a href="<?php echo base_url; ?>views/manager/stock.php"><i class="fa fa-table fa-fw"></i> Stock</a>
                </li>
                <li>
                    <a href="<?php echo base_url; ?>views/manager/productList.php"><i class="fa fa-table fa-fw"></i> Product List</a>
                </li>
                <li>
                    <a href="<?php echo base_url; ?>views/manager/productTrashedList.php"><i class="fa fa-table fa-fw"></i> Product Trashed List</a>
                </li>
                <li>
                    <a href="<?php echo base_url; ?>views/manager/addProduct.php"><i class="fa fa-edit fa-fw"></i> Add Product</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
