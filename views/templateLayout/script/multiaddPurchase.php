<script>
    $(document).ready(function() {
        var max_fields      = 1000; //maximum input boxes allowed
        var wrapper         = $("#input_fields_wrap"); //Fields wrapper
        var add_button      = $("#add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><div class="col-lg-7 col-md-7"><div class="form-group"><select class="form-control" name="product_id[]"><option value="selectProduct">Select Product</option><?php foreach($allData as $oneData){ echo "<option value=\"$oneData->id\">$oneData->product_name</option>";
                }?></select></div></div> <div class="col-lg-4 col-md-4"> <div class="form-group"> <div><input class="form-control" type="number" name="quantity[]" required></div></div> </div><a href="#" class="remove_field btn btn-danger col-md-1 col-lg-1"><i class="fa fa-times" aria-hidden="true"></i></a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>

<script type="text/javascript">
    function valid() {
        var city=document.getElementById("city").value;
        if(city=="selectProduct")
        {
            alert("All fields need to fill up!");
            return false;
        }
    }
</script>
