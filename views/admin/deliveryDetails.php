<?php
session_start();
require_once("../../vendor/autoload.php");
include('../templateLayout/information.php');
use App\Authentication;
use App\Utility\Utility;
if($_SESSION['role_status']==0){
    $auth= new Authentication();
    $status = $auth->setData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../panel/login.php');
        return;
    }
}
else {
    Utility::redirect('../panel/login.php');
}
use App\Delivery_details;
$object= new Delivery_details();
$object->setData($_GET);
$allData=$object->showdetails();
$masterData=$object->showmaster();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title;?></title>
    <?php include('../templateLayout/css/tableCss.php');?>


</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include ('../templateLayout/adminNavigation.php');?>
    <!-- Navigation -->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Delivery List</h1>
            </div>
            <?php

            use App\Message\Message;


            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <div class='alert alert-info alert-dismissable' id='message' style='color: white; background: #6d86d3; text-align: center; font-family: Pristina; font-weight: 200 ;font-size: 20px;'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
                                        $msg.
                                    </div>
                                </div>
                            </div>
                        </div>";
            }

            ?>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Delivery list & price
                    </div>
                    <div class="col-lg-12 col-md-12" style="margin-top: 20px;">
                        <?php
                        foreach($masterData as $oneData) {
                            $date = date("d/m/Y", strtotime("$oneData->order_date"));
                            $time = date('h:i A', strtotime($oneData->order_date));
                            $delivery_date = date("d/m/Y", strtotime("$oneData->delivery_date"));
                            echo "
                            <div class='col-lg-4 col-md-4'>
                                <div class='form-group'>
                                    <label>Invoice Number:</label>
                                    <input class='form-control' value='$oneData->id' disabled>
                                </div>
                            </div>
                            <div class='col-lg-4 col-md-4'>
                                <div class='form-group'>
                                    <label>Shop name:</label>
                                    <input class='form-control' value='$oneData->retailer_shop_name' disabled>
                                </div>
                            </div>
                            <div class='col-lg-4 col-md-4'>
                                <div class='form-group'>
                                    <label>Retailer name:</label>
                                    <input class='form-control' value='$oneData->retailer_name' disabled>
                                </div>
                            </div>
                            <div class='col-lg-4 col-md-4'>
                                <div class='form-group'>
                                    <label>Address:</label>
                                    <input class='form-control' value='$oneData->retailer_address' disabled>
                                </div>
                            </div>
                            <div class='col-lg-4 col-md-4'>
                                <div class='form-group'>
                                    <label>Contact No:</label>
                                    <input class='form-control' value='$oneData->contact_no' disabled>
                                </div>
                            </div>
                            <div class='col-lg-4 col-md-4'>
                                <div class='form-group'>
                                    <label>Delivery Date:</label>
                                    <input class='form-control' value='$delivery_date' disabled>
                                </div>
                            </div>
                            <div class='col-lg-4 col-md-4'>
                                <div class='form-group'>
                                    <label>Ordered Date:</label>
                                    <input class='form-control' value='$date' disabled>
                                </div>
                            </div>
                            <div class='col-lg-4 col-md-4'>
                                <div class='form-group'>
                                    <label>Ordered Time:</label>
                                    <input class='form-control' value='$time' disabled>
                                </div>
                            </div>
                            <div class='col-lg-4 col-md-4'>
                                <div class='form-group'>
                                    <label>Total Price:</label>
                                    <input class='form-control' value='$oneData->total_payment' disabled>
                                </div>
                            </div>
                            ";
                        }
                        ?>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th style='text-align: center'>Serial</th>
                                    <th style='text-align: center'>Product Name</th>
                                    <th style='text-align: center'>quantity</th>
                                    <th style='text-align: center'>total price</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach($allData as $oneData){
                                    echo" <tr>
                                    <td style='text-align: center'>$serial</td>
                                    <td style='text-align: center'>$oneData->product_name</td>
                                    <td style='text-align: center'>$oneData->quantity</td>
                                    <td style='text-align: center'>$oneData->total_price</td>
                                </tr>";
                                    $serial++;
                                }?>



                                </tbody>
                            </table>
                        </div>

                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include('../templateLayout/script/scriptMessege.php');?>
<?php include('../templateLayout/script/tableScript.php');?>


</body>

</html>
