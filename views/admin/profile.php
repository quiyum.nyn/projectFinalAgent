<?php
session_start();
require_once("../../vendor/autoload.php");
include('../templateLayout/information.php');
use App\Authentication;
use App\Utility\Utility;
if($_SESSION['role_status']==0){
    $auth= new Authentication();
    $status = $auth->setData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../panel/login.php');
        return;
    }
}
else {
    Utility::redirect('../panel/login.php');
}
use App\User_info;
$object= new User_info();
$_POST['profileSession']=$_SESSION['email'];
$object->setData($_POST);
$profile=$object->userProfile();

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title;?></title>

    <?php include('../templateLayout/css/css.php');?>
    <style>
        #profile-image1 {
            cursor: pointer;

            width: 100px;
            height: 100px;
            border:2px solid #03b1ce ;}
        .tital{ font-size:16px; font-weight:500;}
        .bot-border{ border-bottom:1px #f8f8f8 solid;  margin:5px 0  5px 0}
    </style>
</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include ('../templateLayout/adminNavigation.php');?>
    <!-- Navigation -->


    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User Information</h1>
            </div>
            <?php

            use App\Message\Message;


            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <div class='alert alert-info alert-dismissable' id='message' style='color: white; background: #6d86d3; text-align: center; font-family: Pristina; font-weight: 200 ;font-size: 20px;'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
                                        $msg.
                                    </div>
                                </div>
                            </div>
                        </div>";
            }

            ?>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">

                            <div class="panel panel-default">
                                <div class="panel-heading">  <h4 >Admin Profile</h4></div>
                                <div class="panel-body">

                                    <div class="box box-info">

                                        <div class="box-body">
                                            <div class="col-sm-12">
                                                <div  align="center"> <img alt="User Pic" src="../../resources/user_photos/<?php echo $profile->picture;?>" id="profile-image1" class="img-circle img-responsive">
                                                </div>
                                                <br>
                                            </div>
                                            <div class="col-sm-6">
                                                <h4 style="color:#00b1b1;">Super Admin</h4></span>
                                                <span><p>Agent Management System</p></span>
                                            </div>
                                            <div class="clearfix"></div>
                                            <hr style="margin:5px 0 5px 0;">
                                            <a href="<?php echo base_url?>views/admin/editProfile.php?id=<?php echo $profile->id?>" class="btn btn-primary">Edit Profile</a>
                                            <a href="<?php echo base_url?>views/admin/resetPassword.php?id=<?php echo $profile->id?>" class="btn btn-primary">Reset Password</a>
                                            <a href="<?php echo base_url?>views/admin/changeProfilePic.php?id=<?php echo $profile->id?>" class="btn btn-primary">Change Profile Picture</a>
                                            <hr style="margin:5px 0 5px 0;">
                                            <div class="col-sm-5 col-xs-6 tital " >Name:</div><div class="col-sm-7 col-xs-6 "><?php echo $profile->full_name;?></div>
                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>
                                            <div class="col-sm-5 col-xs-6 tital " >Father's Name:</div><div class="col-sm-7"><?php echo $profile->father_name;?></div>
                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>
                                            <div class="col-sm-5 col-xs-6 tital " >Address:</div><div class="col-sm-7"><?php echo $profile->address;?></div>
                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>
                                            <div class="col-sm-5 col-xs-6 tital " >Email:</div><div class="col-sm-7"><?php echo $profile->email;?></div>
                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>
                                            <div class="col-sm-5 col-xs-6 tital " >Contact:</div><div class="col-sm-7"><?php echo $profile->contact;?></div>
                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>
                                            <div class="col-sm-5 col-xs-6 tital " >N_ID no:</div><div class="col-sm-7"><?php echo $profile->nid_no;?></div>
                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>
                                            <div class="col-sm-5 col-xs-6 tital " >Religion:</div><div class="col-sm-7">	<?php echo $profile->religion;?></div>
                                            <div class="clearfix"></div>
                                            <div class="bot-border"></div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->

                                    </div>


                                </div>
                            </div>
                        </div>
                        <script>
                            $(function() {
                                $('#profile-image1').on('click', function() {
                                    $('#profile-image-upload').click();
                                });
                            });
                        </script>









                    </div>
                </div>
       <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include('../templateLayout/script/script.php');?>

</body>

</html>
