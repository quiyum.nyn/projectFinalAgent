<?php
session_start();
require_once("../../vendor/autoload.php");
include('../templateLayout/information.php');

use App\Authentication;
use App\Utility\Utility;
if($_SESSION['role_status']==0){
    $auth= new Authentication();
    $status = $auth->setData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../panel/login.php');
        return;
    }
}
else {
    Utility::redirect('../panel/login.php');
}
$object= new \App\Purchase_master();
$allData = $object->showall();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title;?></title>
    <?php include('../templateLayout/css/tableCss.php');?>


</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include ('../templateLayout/adminNavigation.php');?>
    <!-- Navigation -->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Purchase List</h1>
            </div>
            <?php

            use App\Message\Message;


            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <div class='alert alert-info alert-dismissable' id='message' style='color: white; background: #6d86d3; text-align: center; font-family: Pristina; font-weight: 200 ;font-size: 20px;'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
                                        $msg.
                                    </div>
                                </div>
                            </div>
                        </div>";
            }

            ?>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Purchase List
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th style='text-align: center'>Serial</th>
                                    <th style='text-align: center'>Date</th>
                                    <th style='text-align: center'>Time</th>
                                    <th style='text-align: center'>Invoice Number</th>
                                    <th style='text-align: center'>Total Payment</th>
                                    <th style='text-align: center'>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $serial= 1;
                                foreach($allData as $oneData){
                                    $date=date("d/m/Y", strtotime("$oneData->datetime"));
                                    $time = date('h:i A', strtotime($oneData->datetime));
                                    echo" <tr>
                                    <td style='text-align: center'>$serial</td>
                                    <td style='text-align: center'>$date</td>
                                    <td style='text-align: center'>$time</td>
                                    <td style='text-align: center'>$oneData->invoice_number</td>
                                    <td style='text-align: center'>$oneData->total_payment</td>
                                    <td style='text-align: center'>
                                           <a href='../../views/admin/list.php?master_list_id=$oneData->id' class='btn btn-info'>View details</a>
                                    </td>
                                </tr>";
                                    $serial++;
                                }?>



                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include('../templateLayout/script/scriptMessege.php');?>
<?php include('../templateLayout/script/tableScript.php');?>


</body>

</html>
