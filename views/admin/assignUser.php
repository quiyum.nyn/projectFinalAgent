<?php
session_start();
require_once("../../vendor/autoload.php");
include('../templateLayout/information.php');
use App\Authentication;
use App\Utility\Utility;

if($_SESSION['role_status']==0){
    $auth= new Authentication();
    $status = $auth->setData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../panel/login.php');
        return;
    }
}
else {
    Utility::redirect('../panel/login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title;?></title>

    <?php include('../templateLayout/css/css.php');?>

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include ('../templateLayout/adminNavigation.php');?>
    <!-- Navigation -->


    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Assign User</h1>
            </div>
            <?php

            use App\Message\Message;


            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <div class='alert alert-info alert-dismissable' id='message' style='color: white; background: #6d86d3; text-align: center; font-family: Pristina; font-weight: 200 ;font-size: 20px;'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
                                        $msg.
                                    </div>
                                </div>
                            </div>
                        </div>";
            }

            ?>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Assign user with his details.
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <form action="../../controller/admin/processUser.php" method="post" enctype="multipart/form-data">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control" type="text" placeholder="Full Name" name="full_name">
                                        </div>
                                        <div class="form-group">
                                            <label>Father's Name</label>
                                            <input class="form-control" placeholder="Father's Name" type="text" name="father_name">
                                        </div>
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input class="form-control" placeholder="Address" type="text" name="address">
                                        </div>
                                        <div class="form-group">
                                            <label>Contact No</label>
                                            <input class="form-control" placeholder="Contact No" type="text" name="contact">
                                        </div>
                                        <div class="form-group">
                                            <label>N-ID No</label>
                                            <input class="form-control" placeholder="National Id Card No" type="text" name="nid_no">
                                        </div>
                                </div>
                            </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label>Religion</label>
                                        <input class="form-control" type="text" placeholder="Religion" name="religion">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input class="form-control" placeholder="Email" type="email" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input class="form-control" placeholder="Password" type="password" name="password">
                                    </div>
                                    <div class="form-group">
                                        <label>Picture</label>
                                        <input class="form-control" type="file" name="picture" accept="image/*">
                                    </div>
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select name="status" class="form-control">
                                            <option value="manager">Manager</option>
                                            <option value="sr">Sales Representative</option>
                                            <option value="delivery">Delivery Person</option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-info">Add User</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div>
                            </form>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include('../templateLayout/script/script.php');?>

</body>

</html>
