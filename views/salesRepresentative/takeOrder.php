<?php
session_start();
require_once("../../vendor/autoload.php");
include('../templateLayout/information.php');
use App\Authentication;
use App\Utility\Utility;
if($_SESSION['role_status']==2){
    $auth= new Authentication();
    $status = $auth->setData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../panel/login.php');
        return;
    }
}
else {
    Utility::redirect('../panel/login.php');
}
$object= new \App\Product_lookup();
$allData = $object->showall();
?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <script>

    </script>

    <title><?php echo $title;?></title>

    <?php include('../templateLayout/css/css.php');?>
    <script src="<?php echo base_url; ?>resources/bower_components/jquery/dist/jquery.min.js"></script>



</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include ('../templateLayout/srNavigation.php');?>
    <!-- Navigation -->


    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Take Order</h1>
            </div>
            <?php

            use App\Message\Message;


            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "
                        <div class='container'>
                            <div class='row'>
                                <div class='col-md-8 col-md-offset-2'>
                                    <div class='alert alert-info alert-dismissable' id='message' style='color: white; background: #6d86d3; text-align: center; font-family: Pristina; font-weight: 200 ;font-size: 20px;'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>
                                        $msg.
                                    </div>
                                </div>
                            </div>
                        </div>";
            }

            ?>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Order details
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <form action="../../controller/salesRepresentative/orderList.php" method="post">
                                <div class="col-lg-10 col-md-10 col-lg-offset-1 col-md-offset-1"">
                                    <div class="col-lg-4 col-md-4">
                                        <label>Retailer Name:</label>
                                        <div class="form-group">
                                            <input type="text" placeholder="Retailer Name" class="form-control" name="retailer_name" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <label>Retailer's Shop Name:</label>
                                        <div class="form-group">
                                            <input type="text" placeholder="Shop Name" class="form-control" name="retailer_shop_name" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <label>Contact No:</label>
                                        <div class="form-group">
                                            <input type="text" placeholder="Contact No" class="form-control" name="retailer_contact" required>
                                        </div>
                                    </div>
                                <div class="col-lg-8 col-md-8">
                                    <label>Retailer Address:</label>
                                    <div class="form-group">
                                        <input type="text" placeholder="Retailer Address" class="form-control" name="retailer_address" required>
                                    </div>
                                </div>
                                    <div class="col-lg-4 col-md-4">
                                        <label>Delivery Date:</label>
                                        <div class="form-group">
                                            <input type="date" class="form-control" name="delivery_date" required>
                                        </div>
                                    </div>
                                </div>

                        <div class="col-lg-10 col-md-10 col-lg-offset-1 col-md-offset-1" id="input_fields_wrap">
                            <div class="col-lg-7 col-md-7">
                                <p style="text-align: center"><strong>Product Name</strong></p>
                                <div class="form-group">
                                    <select class="form-control" name="ordered_product_id[]">
                                        <option value="selectProduct">Select Product</option>
                                        <?php
                                        foreach($allData as $oneData){
                                            echo"<option value='$oneData->id' >$oneData->product_name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <p style="text-align: center"><strong>Quantity</strong></p>
                                <div class="form-group">
                                    <div><input class="form-control" type="number" name="ordered_quantity[]"required></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-lg-offset-1 col-md-offset-1">
                            <button id="add_field_button" class="btn btn-info">Add Another</button>
                            <input type="submit" class="btn btn-info" value="Submit">
                        </div>

                        </form>
                        <!-- Modal -->

                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include('../templateLayout/script/script.php');?>
<?php include('../templateLayout/script/multiaddOrderlist.php');?>

<script>

    function price() {

        var quantity=document.getElementById("quan").value;
        var select=document.getElementById("p_id");
        var product_id =select.options[select.selectedIndex].value;



        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
                //document.getElementById("price_ajax").value=String(xmlhttp.responseText);
                $('.base_price').val(xmlhttp.responseText);
                console.log(xmlhttp.response)
            }
        }
        xmlhttp.open("GET","get_product.php?p_id="+product_id+"&quantity="+quantity,true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xmlhttp.send();


    }

</script>
<script type="text/javascript">
    $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
</script>

</body>

</html>
