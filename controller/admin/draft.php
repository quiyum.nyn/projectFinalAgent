<?php
session_start();
require_once("../../vendor/autoload.php");
include('../../views/templateLayout/information.php');
$ids=implode(",",$_POST['product_id']);
$invoice=$_POST['invoice_number'];
$product=new \App\Product_lookup();
$unitPrices=$product->getUnitprice($ids);
$products = $product->showalldraft($ids);
$i=0;
$totalprice=0;
$prices=array();
foreach ($unitPrices as $price){
    $price_for_math=intval($price);
    $quantity_for_math=intval($_POST['quantity'][$i]);
    $prices[$i]=$price_for_math*$quantity_for_math;
    $totalprice=$prices[$i]+$totalprice;
    $i++;
}
$quantities=implode(",",$_POST['quantity']);
$price_for_details=implode(",",$prices);
$_SESSION['product_id']=$ids;
$_SESSION['quantity']=$quantities;
$_SESSION['total_price']=$price_for_details;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title;?></title>
    <?php include('../../views/templateLayout/css/tableCss.php');?>
</head>
<body>
<div id="wrapper">
    <!-- Navigation -->
    <?php include ('../../views/templateLayout/adminNavigation.php');?>
    <!-- Navigation -->
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Purchase Draft Details</h1>
            </div>
            <form action="purchaseMasterProcess.php" method="post">
            <div class="col-lg-6 col-md-6">
                <div class="col-lg-6 col-md-6">
                    <label>Invoice Number</label>
                    <div class="form-group">
                        <input type="number" name="invoice_number" class="form-control" value="<?php echo "$invoice";?>" readonly>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <label>Total Ammount</label>
                    <div class="form-group">
                        <input type="number" name= "total_ammount" class="form-control" value="<?php echo "$totalprice";?>" readonly>
                    </div>
                </div>

            </div>


            <!-- /.col-lg-12 -->
            </div>
        <!-- /.row -->
            <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Purchase Draft Details
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th style='text-align: center'>Serial</th>
                                    <th style='text-align: center'>Product Name</th>
                                    <th style='text-align: center'>Unit Price</th>
                                    <th style='text-align: center'>Quantity</th>
                                    <th style='text-align: center'>Total</th>
                                    <th style='text-align: center'>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i=0;
                                $serial=1;
                                foreach ($products as $oneProduct){
                                    $quantity=intval($_POST['quantity'][$i]);

                                    echo "
                                        <tr>
                                            <td style='text-align: center'>$serial</td>
                                            <td style='text-align: center'>$oneProduct</td>
                                            <td style='text-align: center' name='unit_price[]'>$unitPrices[$i]</td>
                                            <td style='text-align: center' name='quantity[]'>$quantity</td>
                                            <td style='text-align: center' name='total_price[]'>$prices[$i]</td>
                                            <td style='text-align: center'><button type='button' class='btn btn-info' data-toggle='modal' data-target='#myModal'>Edit</button></td>
                                        </tr>
                                    ";
                                    $i++;
                                    $serial++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <input type="submit" class="btn btn-info" value="Submit">
        </form>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <p>Some text in the modal.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include('../../views/templateLayout/script/tableScript.php');?>

</body>

</html>
