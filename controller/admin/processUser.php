<?php
require_once("../../vendor/autoload.php");
use App\User_info;
use App\Message\Message;
use App\Utility\Utility;
use App\Authentication;
$auth= new Authentication();
$objectExist= new Authentication();
$status= $objectExist->setData($_POST)->is_exist();
if($status){
    Message::setMessage("Email has been already taken!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    if(isset($_FILES['picture']['name']))
    {
        $picName=time().$_FILES['picture']['name'];
        $tmp_name=$_FILES['picture']['tmp_name'];

        move_uploaded_file($tmp_name,'../../resources/user_photos/'.$picName);
        $_POST['picture_name']=$picName;
    }
    $object= new User_info();
    $object->setData($_POST);
    $object->store();
}

?>