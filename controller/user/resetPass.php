<?php
require_once("../../vendor/autoload.php");
use App\User_info;
use App\Message\Message;
use App\Utility\Utility;
$object= new User_info();
if($_POST['password']==$_POST['con_password']) {
    if($_POST['status']=='delivery'){
        $object->setData($_POST);
        $object->resetPassword();
        Utility::redirect('../../views/delivery/profile.php');
    }
    if($_POST['status']=='manager'){
        $object->setData($_POST);
        $object->resetPassword();
        Utility::redirect('../../views/manager/profile.php');
    }
    if($_POST['status']=='sr'){
        $object->setData($_POST);
        $object->resetPassword();
        Utility::redirect('../../views/salesRepresentative/profile.php');
    }
    if($_POST['status']=='admin'){
        $object->setData($_POST);
        $object->resetPassword();
        Utility::redirect('../../views/admin/profile.php');
    }
}
else{
    Message::setMessage("New Password & Confirm Password should be must same!");
    Utility::redirect('../../views/delivery/resetPassword.php');
}
?>