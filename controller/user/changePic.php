<?php
require_once("../../vendor/autoload.php");
use App\User_info;
use App\Message\Message;
use App\Utility\Utility;
use App\Authentication;
if($_POST['status']=='delivery'){
    if(isset($_FILES['picture']['name']))
    {
        $picName=time().$_FILES['picture']['name'];
        $tmp_name=$_FILES['picture']['tmp_name'];

        move_uploaded_file($tmp_name,'../../resources/user_photos/'.$picName);
        $_POST['picture_name']=$picName;
    }
    $object= new User_info();
    $object->setData($_POST);
    $object->updatePic();
    Utility::redirect('../../views/delivery/profile.php');
}

if($_POST['status']=='manager'){
    if(isset($_FILES['picture']['name']))
    {
        $picName=time().$_FILES['picture']['name'];
        $tmp_name=$_FILES['picture']['tmp_name'];

        move_uploaded_file($tmp_name,'../../resources/user_photos/'.$picName);
        $_POST['picture_name']=$picName;
    }
    $object= new User_info();
    $object->setData($_POST);
    $object->updatePic();
    Utility::redirect('../../views/manager/profile.php');
}
if($_POST['status']=='sr'){
    if(isset($_FILES['picture']['name']))
    {
        $picName=time().$_FILES['picture']['name'];
        $tmp_name=$_FILES['picture']['tmp_name'];

        move_uploaded_file($tmp_name,'../../resources/user_photos/'.$picName);
        $_POST['picture_name']=$picName;
    }
    $object= new User_info();
    $object->setData($_POST);
    $object->updatePic();
    Utility::redirect('../../views/salesRepresentative/profile.php');
}
if($_POST['status']=='admin'){
    if(isset($_FILES['picture']['name']))
    {
        $picName=time().$_FILES['picture']['name'];
        $tmp_name=$_FILES['picture']['tmp_name'];

        move_uploaded_file($tmp_name,'../../resources/user_photos/'.$picName);
        $_POST['picture_name']=$picName;
    }
    $object= new User_info();
    $object->setData($_POST);
    $object->updatePic();
    Utility::redirect('../../views/admin/profile.php');
}
?>