<?php
    require_once("../../vendor/autoload.php");
    use App\User_info;
    use App\Utility\Utility;
    $object= new User_info();

    if($_POST['status']=='delivery'){
        $object->setData($_POST);
        $object->update();
        Utility::redirect('../../views/delivery/profile.php');
    }

    if($_POST['status']=='manager'){
        $object->setData($_POST);
        $object->update();
        Utility::redirect('../../views/manager/profile.php');
    }
    if($_POST['status']=='sr'){
        $object->setData($_POST);
        $object->update();
        Utility::redirect('../../views/salesRepresentative/profile.php');
    }
    if($_POST['status']=='admin'){
        $object->setData($_POST);
        $object->update();
        Utility::redirect('../../views/admin/profile.php');
    }
?>