<?php
if(!isset($_SESSION) )session_start();
include_once('../../vendor/autoload.php');
use App\Authentication;
use App\Utility\Utility;
use App\Message\Message;
$auth= new Authentication();

$admin= $auth->setData($_POST)->admin();
$manager= $auth->setData($_POST)->manager();
$sr= $auth->setData($_POST)->sr();
$delivery= $auth->setData($_POST)->delivery();





if($admin){
    $_SESSION['role_status']=0;
    $_SESSION['email']=$_POST['email'];
    Message::setMessage("Success! You have log in Successfully!");
    Utility::redirect('../../views/admin/profile.php');
}
else if ($manager){
    $_SESSION['role_status']=1;
    $_SESSION['email']=$_POST['email'];
    Message::setMessage("Success! You have log in Successfully!");
    Utility::redirect('../../views/manager/profile.php');
}
else if ($sr){
    $_SESSION['role_status']=2;
    $_SESSION['email']=$_POST['email'];
    Message::setMessage("Success! You have log in Successfully!");
    Utility::redirect('../../views/salesRepresentative/profile.php');
}
else if ($delivery){
    $_SESSION['role_status']=3;
    $_SESSION['email']=$_POST['email'];
    Message::setMessage("Success! You have log in Successfully!");
    Utility::redirect('../../views/delivery/profile.php');
}
else{
    Message::setMessage("Email and password doesn't match!");
    Utility::redirect('../../views/panel/login.php');

}


