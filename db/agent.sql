-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2017 at 01:12 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agent`
--

-- --------------------------------------------------------

--
-- Table structure for table `delivery_details`
--

CREATE TABLE `delivery_details` (
  `id` int(22) NOT NULL,
  `delivery_master_id` int(22) NOT NULL,
  `product_id` int(22) NOT NULL,
  `quantity` int(22) NOT NULL,
  `total_price` int(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_details`
--

INSERT INTO `delivery_details` (`id`, `delivery_master_id`, `product_id`, `quantity`, `total_price`) VALUES
(20, 10000, 6, 45, 23625),
(21, 10000, 11, 243, 9720),
(22, 10000, 18, 789, 15780),
(23, 10000, 2, 45, 675),
(24, 10001, 6, 45, 2025),
(25, 10001, 9, 46, 644),
(26, 10001, 19, 25, 5000),
(27, 10001, 5, 14, 140),
(28, 10001, 23, 45, 1800),
(29, 10001, 24, 47, 2350),
(30, 10002, 15, 2, 112),
(31, 10002, 16, 3, 114),
(32, 10002, 18, 4, 60),
(33, 10003, 13, 4, 560),
(34, 10003, 24, 6, 1200),
(35, 10003, 9, 67, 14070),
(36, 10003, 4, 3, 150),
(37, 10004, 13, 12, 2520),
(38, 10004, 18, 23, 345),
(39, 10005, 15, 2, 112),
(40, 10005, 16, 23, 874),
(41, 10006, 11, 32, 1280);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_master`
--

CREATE TABLE `delivery_master` (
  `id` int(11) NOT NULL,
  `retailer_name` varchar(256) NOT NULL,
  `retailer_shop_name` varchar(256) NOT NULL,
  `retailer_address` varchar(500) NOT NULL,
  `contact_no` varchar(25) NOT NULL,
  `order_date` datetime NOT NULL,
  `delivery_date` date NOT NULL,
  `total_payment` int(22) NOT NULL,
  `status` varchar(256) NOT NULL DEFAULT 'ordered'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_master`
--

INSERT INTO `delivery_master` (`id`, `retailer_name`, `retailer_shop_name`, `retailer_address`, `contact_no`, `order_date`, `delivery_date`, `total_payment`, `status`) VALUES
(10000, 'Didarul Alom', 'Emdad Store', 'Chaktai', '01826132308', '2017-03-08 15:04:32', '2017-03-19', 49800, 'delivered'),
(10001, 'Didarul Alam', 'Emdad Store', 'Chaktai', '01826132308', '2017-03-08 17:22:41', '2017-03-10', 11959, 'delivered'),
(10002, 'Irfan', 'Irfan Store', 'Irfan heights,Dhaka', '01xxxxxxxx', '2017-03-14 18:01:54', '2017-03-14', 286, 'delivered'),
(10003, 'dfgfgfgf', 'cdfdcvc', 'cxcxcc', '8977553', '2017-03-15 04:42:52', '2017-04-13', 15980, 'delivered'),
(10004, 'irfan', 'irfan store', 'Irfan Heights', '12345678', '2017-03-15 04:58:27', '2017-12-01', 2865, 'delivered'),
(10005, 'wqeqw', 'asdasd', 'asdasd', '2133q', '2017-03-15 05:00:07', '2017-12-01', 986, 'delivered'),
(10006, 'exfsdf', 'dsfsdf', 'asdfasd', '1245235', '2017-03-15 05:03:15', '2017-12-01', 1280, 'delivered');

-- --------------------------------------------------------

--
-- Stand-in structure for view `inventory`
--
CREATE TABLE `inventory` (
`productId` int(20)
,`productName` varchar(500)
,`DebitQuantity` decimal(41,0)
,`CreditQuantity` decimal(41,0)
,`BalanceQuantity` decimal(42,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `product_lookup`
--

CREATE TABLE `product_lookup` (
  `id` int(11) NOT NULL,
  `product_name` varchar(500) NOT NULL,
  `unit_price` int(25) NOT NULL,
  `status` varchar(256) NOT NULL DEFAULT 'available'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_lookup`
--

INSERT INTO `product_lookup` (`id`, `product_name`, `unit_price`, `status`) VALUES
(1, 'Fresh powder milk - 2 kg', 1100, 'available'),
(2, 'Fresh Powder Milk - 1 kg', 525, 'available'),
(3, 'Fresh Powder Milk - 500 gm', 275, 'available'),
(4, 'Fresh Powder Milk - 250 gm', 140, 'available'),
(5, 'Fresh Powder Milk - 75 gm', 45, 'available'),
(6, 'Fresh Powder Milk - 25 gm', 14, 'available'),
(7, 'Pure milk - 2 kg', 1100, 'available'),
(8, 'Pure milk - 1 kg', 575, 'available'),
(9, 'Premium Leaf Tea - 500 gm', 200, 'available'),
(10, 'Premium Leaf Tea - 200 gm', 80, 'available'),
(11, 'Premium Leaf Tea - 100 gm', 40, 'available'),
(12, 'Premium Leaf Tea - 50 gm', 20, 'available'),
(13, 'Fresh Pilule Leaf Tea - 500 gm ', 210, 'available'),
(14, 'Fresh Mineral Water - 5 liter ', 90, 'available'),
(15, 'Fresh Mineral Water - 3 liter ', 56, 'available'),
(16, 'Fresh Mineral Water - 2 liter ', 38, 'available'),
(17, 'Fresh Mineral Water - 1 liter ', 20, 'available'),
(18, 'Fresh Mineral Water - 500 gm', 15, 'available'),
(19, 'Fresh Mineral Water - 250 gm', 10, 'available'),
(20, 'Fresh Sugar - 1 kg', 65, 'available'),
(21, 'Fresh Sugar - 500 gm', 35, 'available'),
(22, 'Fresh Flour - 1 kg', 42, 'available'),
(23, 'Fresh Coarse Flour - 1 kg', 40, 'available'),
(24, 'Fresh Suzie- 500 gm', 50, 'available');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_details`
--

CREATE TABLE `purchase_details` (
  `id` int(11) NOT NULL,
  `purchase_master_id` int(20) NOT NULL,
  `product_id` int(20) NOT NULL,
  `quantity` int(20) NOT NULL,
  `total_price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_details`
--

INSERT INTO `purchase_details` (`id`, `purchase_master_id`, `product_id`, `quantity`, `total_price`) VALUES
(62, 21, 12, 25, 27500),
(63, 21, 9, 45, 9000),
(64, 21, 1, 25, 500),
(65, 21, 18, 78, 1170),
(66, 22, 4, 5, 700),
(67, 22, 11, 6, 240),
(68, 22, 12, 4, 80),
(69, 23, 6, 400, 4400),
(70, 23, 7, 8, 112),
(71, 23, 8, 7, 7700),
(72, 23, 14, 8, 4600),
(73, 23, 19, 10, 200),
(74, 23, 12, 78, 7020),
(75, 23, 1, 5, 50),
(76, 23, 22, 7, 294),
(77, 23, 23, 8, 320),
(78, 24, 8, 5, 2875),
(79, 24, 14, 7, 1470),
(80, 24, 13, 8, 720),
(81, 24, 16, 7, 266),
(82, 25, 15, 34, 1904),
(83, 25, 16, 45, 1710),
(84, 26, 15, 34, 37400),
(85, 26, 1, 100, 2000),
(86, 26, 13, 45, 9450),
(87, 26, 12, 345, 19320),
(88, 27, 1, 245, 269500),
(89, 27, 16, 45, 1710),
(90, 27, 17, 56, 1120),
(91, 28, 2, 1000, 525000),
(92, 28, 3, 1000, 275000),
(93, 28, 4, 1000, 140000),
(94, 28, 5, 1000, 45000),
(95, 28, 6, 1000, 14000),
(96, 28, 7, 1000, 1100000),
(97, 28, 8, 1000, 575000),
(98, 28, 9, 1000, 200000),
(99, 28, 10, 1000, 80000),
(100, 28, 11, 1000, 40000),
(101, 28, 12, 1000, 20000),
(102, 28, 13, 1000, 210000),
(103, 28, 14, 1000, 90000),
(104, 28, 15, 1000, 56000),
(105, 29, 18, 1000, 56000),
(106, 29, 19, 1000, 38000),
(107, 29, 16, 1000, 15000),
(108, 29, 15, 1000, 10000),
(109, 30, 24, 1000, 42000),
(110, 30, 23, 1000, 40000),
(111, 30, 22, 1000, 50000),
(112, 31, 15, 34, 1904),
(113, 32, 15, 12, 672);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_master`
--

CREATE TABLE `purchase_master` (
  `id` int(11) NOT NULL,
  `invoice_number` int(25) NOT NULL,
  `datetime` datetime NOT NULL,
  `total_payment` int(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_master`
--

INSERT INTO `purchase_master` (`id`, `invoice_number`, `datetime`, `total_payment`) VALUES
(21, 12564, '2017-03-04 15:11:52', 38170),
(22, 23243234, '2017-03-04 15:17:16', 1020),
(23, 4563, '2017-03-04 16:24:56', 24696),
(24, 4565415, '2017-03-04 17:20:53', 5331),
(25, 123232, '2017-03-12 00:49:21', 3614),
(26, 123232, '2017-03-12 00:58:42', 68170),
(27, 123345, '2017-03-13 17:09:26', 272330),
(28, 1245367, '2017-03-15 03:12:39', 3370000),
(29, 234465, '2017-03-15 03:13:31', 119000),
(30, 456464, '2017-03-15 03:14:12', 132000),
(31, 213451, '2017-03-15 04:55:50', 1904),
(32, 1234568, '2017-03-15 05:21:02', 672);

-- --------------------------------------------------------

--
-- Stand-in structure for view `st`
--
CREATE TABLE `st` (
`prod_id` int(20)
,`dr_quantity` bigint(20)
,`cr_quantity` bigint(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `full_name` varchar(256) CHARACTER SET latin1 NOT NULL,
  `father_name` varchar(256) CHARACTER SET latin1 NOT NULL,
  `address` varchar(500) CHARACTER SET latin1 NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 NOT NULL,
  `contact` varchar(50) CHARACTER SET latin1 NOT NULL,
  `picture` varchar(100) CHARACTER SET latin1 NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 NOT NULL,
  `nid_no` varchar(50) CHARACTER SET latin1 NOT NULL,
  `religion` varchar(50) CHARACTER SET latin1 NOT NULL,
  `status` varchar(50) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `full_name`, `father_name`, `address`, `email`, `contact`, `picture`, `password`, `nid_no`, `religion`, `status`) VALUES
(18, 'Abdullah Al Quiyum', 'Md Abu Sayed', 'East Nasirabad, Chittagong', 'quiyum.nyn@gmail.com', '01826132308', '1489526741quiyum.jpg', 'e10adc3949ba59abbe56e057f20f883e', '19921592439000090', 'Islam', 'admin'),
(19, 'Mamunur Rashid', 'Mominullah', 'Lalkhan Bazar', 'mamun.pciu127@gmail.com', '01772040475', '1489491776shahid.jpg', '20cb9506d935018c52e15f37ab1e2db0', '19951516185000284', 'Islam', 'manager'),
(20, 'Md Erfan', 'Md Ismail Chy', 'South Khulsi', 'erfanchy95@gmail.com', '01859147902', '1489530435shohel.jpg', 'e10adc3949ba59abbe56e057f20f883e', '19921592439000090', 'Islam', 'manager'),
(21, 'Ahad', 'Rahat Ali', 'Boiltoli Boroma', 'delivery@gmail.com', '01859147902', '1489529738mishu.jpg', '7108537956afa2a526f96cc9da7b0c36', '122324457674778', 'Islam', 'delivery'),
(22, 'Fahad', 'Mohsin Alom', 'South Khulsi', 'sales@gmail.com', '01859147902', '1489530781mishu.jpg', 'e10adc3949ba59abbe56e057f20f883e', '233445566756522', 'Islam', 'sr'),
(23, 'qwe', 'weqwe', 'wqewqe', 'sdfsdf@sfsd', '23134', '1489532025tusar.jpg', '202cb962ac59075b964b07152d234b70', '12321', 'sd', 'manager'),
(24, 'Didarul Alam', 'Uncle', 'Panchlaish', 'didar@gmail.com', '01xxxxxxxx', '1489533030policeman-512.png', 'e10adc3949ba59abbe56e057f20f883e', '1921321313123', 'Islam', 'manager'),
(25, 'Abu Nayam Mohammad Imtiaj', 'Abu Nayam Mohammad Imtiaj', 'Burischar', 'anmohammad.imtiaj@gmail.com', '01821xxxxxx', '14895331911466944417_3.jpg', 'e10adc3949ba59abbe56e057f20f883e', '199312232321', 'Islam', 'manager');

-- --------------------------------------------------------

--
-- Structure for view `inventory`
--
DROP TABLE IF EXISTS `inventory`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `inventory`  AS  select `a`.`prod_id` AS `productId`,`b`.`product_name` AS `productName`,sum(`a`.`dr_quantity`) AS `DebitQuantity`,sum(`a`.`cr_quantity`) AS `CreditQuantity`,(sum(`a`.`dr_quantity`) - sum(`a`.`cr_quantity`)) AS `BalanceQuantity` from (`st` `a` join `product_lookup` `b`) where (`a`.`prod_id` = `b`.`id`) group by `a`.`prod_id` ;

-- --------------------------------------------------------

--
-- Structure for view `st`
--
DROP TABLE IF EXISTS `st`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `st`  AS  select `purchase_details`.`product_id` AS `prod_id`,`purchase_details`.`quantity` AS `dr_quantity`,0 AS `cr_quantity` from `purchase_details` union all select `delivery_details`.`product_id` AS `prod_id`,0 AS `dr_quantity`,`delivery_details`.`quantity` AS `cr_quantity` from `delivery_details` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `delivery_details`
--
ALTER TABLE `delivery_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_master`
--
ALTER TABLE `delivery_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_lookup`
--
ALTER TABLE `product_lookup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_details`
--
ALTER TABLE `purchase_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_master`
--
ALTER TABLE `purchase_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `delivery_details`
--
ALTER TABLE `delivery_details`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `delivery_master`
--
ALTER TABLE `delivery_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10007;
--
-- AUTO_INCREMENT for table `product_lookup`
--
ALTER TABLE `product_lookup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `purchase_details`
--
ALTER TABLE `purchase_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `purchase_master`
--
ALTER TABLE `purchase_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
