<?php
/**
 * Created by PhpStorm.
 * User: Quiyum
 * Date: 3/1/2017
 * Time: 1:20 AM
 */

namespace App;

use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Purchase_master extends Database
{
    public $id;
    public $invoice_number;
    public $total_price;
    public $date;

    public function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id= $allPostData['id'];
        }

        if(array_key_exists("invoice_number",$allPostData)){
            $this->invoice_number= $allPostData['invoice_number'];
        }
        if(array_key_exists("total_ammount",$allPostData)){
            $this->total_price= $allPostData['total_ammount'];
        }

    }
    public function demo(){
        var_dump($this->id);
        die();
    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $arrData = array($this->invoice_number,$this->date,$this->total_price,);
        $query= "INSERT INTO purchase_master (invoice_number,datetime,total_payment) VALUES (?,?,?)";
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);
        if($result){
            Message::setMessage("Success! Data stored successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('../../views/manager/purchaseList.php');
    }
    public function showall(){

        $sql = "Select * from purchase_master";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showList(){

        $sql = "SELECT purchase_details.product_id,purchase_details.quantity,purchase_details.total_price FROM purchase_details, purchase_master WHERE purchase_details.purchase_master_id=$this->id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
        
    }

    public function storeadmin(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $arrData = array($this->invoice_number,$this->date,$this->total_price,);
        $query= "INSERT INTO purchase_master (invoice_number,datetime,total_payment) VALUES (?,?,?)";
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);
        if($result){
            Message::setMessage("Success! Data stored successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('../../views/admin/purchaseList.php');
    }

    public function purchaseQuantity(){
        $sql = "SELECT pq.totalQuantity,pl.product_name from purchase_quantity as pq INNER JOIN product_lookup as pl ON pl.id=pq.product_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function deliveryQuantity(){
        $sql = "SELECT pq.totalQuantity,pl.product_name from delivery_quantity as pq INNER JOIN product_lookup as pl ON pl.id=pq.product_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}