<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 3/6/2017
 * Time: 5:24 PM
 */

namespace App;
use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class Delivery_master extends Database
{
    public $id;
    public $retailer_name;
    public $retailer_shop_name;
    public $retailer_address;
    public $contact_no;
    public $order_date;
    public $delivery_date;
    public $total_payment;
    public $list_id;

    public function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id= $allPostData['id'];
        }
        if(array_key_exists("retailer_name",$allPostData)){
            $this->retailer_name= $allPostData['retailer_name'];
        }
        if(array_key_exists("retailer_shop_name",$allPostData)){
            $this->retailer_shop_name= $allPostData['retailer_shop_name'];
        }
        if(array_key_exists("retailer_contact",$allPostData)){
            $this->contact_no= $allPostData['retailer_contact'];
        }
        if(array_key_exists("retailer_address",$allPostData)){
            $this->retailer_address= $allPostData['retailer_address'];
        }
        if(array_key_exists("delivery_date",$allPostData)){
            $this->delivery_date= $allPostData['delivery_date'];
        }
        if(array_key_exists("total_amount",$allPostData)){
            $this->total_payment= $allPostData['total_amount'];
        }
        if(array_key_exists("master_list_id",$allPostData)){
            $this->list_id= $allPostData['master_list_id'];
        }
    }

    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->order_date=$date;
        $arrData = array($this->retailer_name,$this->retailer_shop_name,$this->retailer_address,$this->contact_no,$this->order_date,$this->delivery_date,$this->total_payment);
        $query= "INSERT INTO delivery_master (retailer_name,retailer_shop_name,	retailer_address,contact_no,order_date,delivery_date,total_payment) VALUES (?,?,?,?,?,?,?)";
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

     
    }

    public function showall(){

        $sql = "Select * from delivery_master where status='ordered'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showdelivery(){

        $sql = "Select * from delivery_master where status='delivered'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function delivery(){
        $arrData  =  array("delivered");

        $query= 'UPDATE delivery_master SET status = ? WHERE id='.$this->list_id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Order has been delivered successfully!");
        }
        else{
            Message::setMessage("Failed! Order has not been delivered!");
        }

        Utility::redirect('../../views/delivery/deliveryList.php');

    }

}