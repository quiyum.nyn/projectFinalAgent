<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 3/11/2017
 * Time: 3:12 PM
 */

namespace App;
use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class User_info extends Database
{
    public $id;
    public $name;
    public $f_name;
    public $address;
    public $email;
    public $contact;
    public $picture;
    public $password;
    public $nid_no;
    public $religion;
    public $status;
    public $profileSession;
    public $tempLoacation;
    private $success;
    public $userID;

    public function setData($allPostData)
    {
        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }

        if (array_key_exists("full_name", $allPostData)) {
            $this->name = $allPostData['full_name'];
        }

        if (array_key_exists("father_name", $allPostData)) {
            $this->f_name = $allPostData['father_name'];
        }
        if (array_key_exists("address", $allPostData)) {
            $this->address = $allPostData['address'];
        }
        if (array_key_exists("contact", $allPostData)) {
            $this->contact = $allPostData['contact'];
        }
        if (array_key_exists("nid_no", $allPostData)) {
            $this->nid_no = $allPostData['nid_no'];
        }
        if (array_key_exists("religion", $allPostData)) {
            $this->religion = $allPostData['religion'];
        }
        if (array_key_exists("email", $allPostData)) {
            $this->email = $allPostData['email'];
        }
        if (array_key_exists("password", $allPostData)) {
            $this->password = md5($allPostData['password']);
        }
        if (array_key_exists("picture_name", $allPostData)) {
            $this->picture = $allPostData['picture_name'];
        }
        if (array_key_exists("status", $allPostData)) {
            $this->status = $allPostData['status'];
        }

        if (array_key_exists("profileSession", $allPostData)) {
            $this->profileSession = $allPostData['profileSession'];
        }

        if (array_key_exists("userID", $allPostData)) {
            $this->userID = $allPostData['userID'];
        }
    }

    public function store(){
        $arrData  =  array($this->name,$this->f_name,$this->address,$this->email,$this->contact,$this->picture,$this->password,$this->nid_no,$this->religion,$this->status);

        $query= 'INSERT INTO user_info (full_name,father_name,address,email,contact,picture,password,nid_no,religion,status) VALUES (?,?,?,?,?,?,?,?,?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            // die();
            Message::setMessage("Success! User info added successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }

        Utility::redirect('../../views/admin/assignUser.php');

    }
    public function userProfile(){

            $sql = "SELECT * FROM user_info WHERE email='" . $this->profileSession . "'";
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            return $STH->fetch();

    }

    public function setProPic($allFileDate=null){

        if(array_key_exists("Picture",$allFileDate)){


            $this->picture= $allFileDate['Picture']['name'];
            $this->tempLoacation= $allFileDate['Picture']['tmp_name'];
            $src= "image/".time().$this->picture;
            $this->success = move_uploaded_file($this->tempLoacation, $src);
            $this->error = $allFileDate['Picture']['error'];
        }
    }

    public function update(){
            $arrData  =  array($this->name, $this->f_name,$this->address,$this->email,$this->contact,$this->nid_no,$this->religion);
            $query= 'UPDATE user_info SET full_name = ?,father_name=?,address=?,email=?,contact=?,nid_no=?,religion=? WHERE id='.$this->userID;
            $STH = $this->DBH->prepare($query);
            $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Profile has been updated successfully!");
        }
        else{
            Message::setMessage("Failed! Profile has not been updated!");
        }
        
        

    }
    public function updatePic(){
        $arrData  =  array($this->picture);
        $query= 'UPDATE user_info SET picture=? WHERE id='.$this->userID;
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Profile picture has been updated successfully!");
        }
        else{
            Message::setMessage("Failed! Profile picture has not been updated!");
        }
        

    }
    public function resetPassword(){
        $arrData  =  array($this->password);
        $query= 'UPDATE user_info SET password=? WHERE id='.$this->userID;
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Password has been reset successfully!");
        }
        else{
            Message::setMessage("Failed! Password has not been reset!");
        }

      

    }
}