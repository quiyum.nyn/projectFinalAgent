<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 2/25/2017
 * Time: 4:44 PM
 */

namespace App;
use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Purchase_details extends Database
{
    public $id;
    public $invoice_number;
    public $product_name;
    public $quantity;
    public $total_price;
    public $product_id;
    public $master_id;
    public $purchase_master_id;
    public $list_id;

    public function setData($allPostData){

        if(array_key_exists("id",$allPostData)){
            $this->id= $allPostData['id'];
        }

        if(array_key_exists("invoice_number",$allPostData)){
            $this->invoice_number= $allPostData['invoice_number'];
        }

        if(array_key_exists("product_name",$allPostData)){
            $this->product_name= $allPostData['product_name'];
        }
        if(array_key_exists("product_id",$allPostData)){
            $this->product_id= $allPostData['product_id'];
        }
        if(array_key_exists("quantity",$allPostData)){
            $this->quantity= $allPostData['quantity'];
        }
        if(array_key_exists("total_price",$allPostData)){
            $this->total_price= $allPostData['total_price'];
        }
        if(array_key_exists("master_list_id",$allPostData)){
            $this->list_id= $allPostData['master_list_id'];
        }
        
        return $this;
    }
    public function store(){
        $selcet_masterid="SELECT id FROM `purchase_master` order by id DESC Limit 1";
        $STH = $this->DBH->query($selcet_masterid);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->master_id=$row['id'];
        
        $pr_id=explode(",",$this->product_id);
        $quan=explode(",",$this->quantity);
        $price_and_unit=explode(",",$this->total_price);

        $count=0;
            foreach($pr_id as $pr){
                $query= "INSERT INTO purchase_details (purchase_master_id,product_id,quantity,total_price) VALUES (?,?,?,?)";
                $STH = $this->DBH->prepare($query);
                $STH->bindParam(1,$this->master_id);
                $STH->bindParam(2,$pr_id[$count]);
                $STH->bindParam(3,$quan[$count]);
                $STH->bindParam(4,$price_and_unit[$count]);

                $result = $STH->execute();
                if($result){
                    Message::setMessage("Success! Product details added successfully!");
                }
                else{
                    Message::setMessage("Failed! Data has not been stored!");
                }
                $count++;
        }
        Utility::redirect('../../views/manager/purchaseList.php');
    }
    public function showdetails(){
        $sql = "SELECT purchase_details.purchase_master_id,product_lookup. product_name,purchase_details.quantity,purchase_details.total_price FROM purchase_details,product_lookup WHERE purchase_details.purchase_master_id=$this->list_id AND purchase_details.product_id=product_lookup.id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
        Utility::redirect('../../views/manager/list.php');
    }
    public function showmaster(){
        $sql = "SELECT * FROM purchase_master WHERE id=$this->list_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function storeadmin(){
        $selcet_masterid="SELECT id FROM `purchase_master` order by id DESC Limit 1";
        $STH = $this->DBH->query($selcet_masterid);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->master_id=$row['id'];

        $pr_id=explode(",",$this->product_id);
        $quan=explode(",",$this->quantity);
        $price_and_unit=explode(",",$this->total_price);

        $count=0;
        foreach($pr_id as $pr){
            $query= "INSERT INTO purchase_details (purchase_master_id,product_id,quantity,total_price) VALUES (?,?,?,?)";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1,$this->master_id);
            $STH->bindParam(2,$pr_id[$count]);
            $STH->bindParam(3,$quan[$count]);
            $STH->bindParam(4,$price_and_unit[$count]);

            $result = $STH->execute();
            if($result){
                Message::setMessage("Success! Product details added successfully!");
            }
            else{
                Message::setMessage("Failed! Data has not been stored!");
            }
            $count++;
        }
        Utility::redirect('../../views/admin/purchaseList.php');
    }


}