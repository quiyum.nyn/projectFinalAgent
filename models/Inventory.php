<?php
/**
 * Created by PhpStorm.
 * User: Quiyum
 * Date: 3/15/2017
 * Time: 1:46 AM
 */

namespace App;
use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class Inventory extends Database
{
    public function stock(){
        $sql = "SELECT * FROM inventory";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}