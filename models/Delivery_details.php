<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 3/6/2017
 * Time: 5:25 PM
 */

namespace App;
use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class Delivery_details extends Database
{
    public $id;
    public $delivery_master_id;
    public $product_id;
    public $quantity;
    public $total_price;
    public $list_id;

    public function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id= $allPostData['id'];
        }
        if(array_key_exists("ordered_product_id",$allPostData)){
            $this->product_id= $allPostData['ordered_product_id'];
        }
        if(array_key_exists("ordered_quantity",$allPostData)){
            $this->quantity= $allPostData['ordered_quantity'];
        }
        if(array_key_exists("ordered_total_price",$allPostData)){
            $this->total_price= $allPostData['ordered_total_price'];
        }
        if(array_key_exists("master_list_id",$allPostData)){
            $this->list_id= $allPostData['master_list_id'];
        }
        return $this;
    }

    public function store(){
        $selcet_delivery_id="SELECT id FROM `delivery_master` order by id DESC Limit 1";
        $STH = $this->DBH->query($selcet_delivery_id);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->delivery_master_id=$row['id'];

        $pr_id=explode(",",$this->product_id);
        $quan=explode(",",$this->quantity);
        $price_and_unit=explode(",",$this->total_price);

        $count=0;
        foreach($pr_id as $pr){
            $query= "INSERT INTO delivery_details (delivery_master_id,product_id,quantity,total_price) VALUES (?,?,?,?)";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1,$this->delivery_master_id);
            $STH->bindParam(2,$pr_id[$count]);
            $STH->bindParam(3,$quan[$count]);
            $STH->bindParam(4,$price_and_unit[$count]);

            $result = $STH->execute();
            if($result){
                Message::setMessage("Success! Order taken successfully!");
            }
            else{
                Message::setMessage("Failed! order taken failed!");
            }
            $count++;
        }
        Utility::redirect('../../views/salesRepresentative/orderedList.php');
    }

    public function showdetails(){
        $sql = "SELECT delivery_details.delivery_master_id,product_lookup.product_name,delivery_details.quantity,delivery_details.total_price FROM delivery_details,product_lookup WHERE delivery_details.delivery_master_id=$this->list_id AND delivery_details.product_id=product_lookup.id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
        Utility::redirect('../../views/salesRepresentative/orderdetails.php');
    }
    public function showmaster(){
        $sql = "SELECT * FROM delivery_master WHERE id=$this->list_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}